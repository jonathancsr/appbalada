import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Geolocation } from '@ionic-native/geolocation';
import { CadastroBaladaPage } from '../pages/cadastro-balada/cadastro-balada';
import { InputMaskModule } from 'ionic-input-mask';

import { BrMaskerModule } from 'brmasker-ionic-3';
import { CadastroUsuarioPage } from '../pages/cadastro-usuario/cadastro-usuario';
import { LoginPage } from '../pages/login/login';
import { ValidacaoPage } from '../pages/validacao/validacao';
import { EventoPage } from '../pages/evento/evento';
import { EsqueciSenhaPage} from '../pages/esqueci-senha/esqueci-senha';
import { FeedbackPage } from '../pages/feedback/feedback';

import { HttpClientModule } from  '@angular/common/http';

import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64';

import { HTTP } from '@ionic-native/http';
import { HttpModule } from '@angular/http';




@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CadastroBaladaPage,
    CadastroUsuarioPage,
    LoginPage,
    ValidacaoPage,
    EventoPage,
    FeedbackPage,
    EsqueciSenhaPage
  ],
  imports: [
    BrowserModule,
    InputMaskModule,
    BrMaskerModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp),

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CadastroBaladaPage,
    CadastroUsuarioPage,
    LoginPage,
    ValidacaoPage,
    EventoPage,
    FeedbackPage,
    EsqueciSenhaPage
  ],
  providers: [
    StatusBar,
    ImagePicker,
    Base64,
    UniqueDeviceID,
    HTTP,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
     Geolocation
  ]
})
export class AppModule {}
