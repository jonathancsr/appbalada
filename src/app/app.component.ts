import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { CadastroBaladaPage } from '../pages/cadastro-balada/cadastro-balada';
import { CadastroUsuarioPage } from '../pages/cadastro-usuario/cadastro-usuario';
import { LoginPage } from '../pages/login/login';
import { ValidacaoPage } from '../pages/validacao/validacao';
import { EventoPage } from '../pages/evento/evento';
import { EsqueciSenhaPage} from '../pages/esqueci-senha/esqueci-senha';
import { FeedbackPage } from '../pages/feedback/feedback';

 
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Cadastro de Baladas', component: CadastroBaladaPage },
      {title: 'Cadastro de Usuário', component: CadastroUsuarioPage},
      {title: 'Login', component: LoginPage},
      {title: 'Validacao', component: ValidacaoPage},
      {title: 'Evento', component: EventoPage},
      {title: 'Esqueci senha', component: EsqueciSenhaPage},
      {title: 'FeedBack', component: FeedbackPage},
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
