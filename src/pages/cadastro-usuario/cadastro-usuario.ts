import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as $ from 'jquery';
import { ValidacaoPage } from '../validacao/validacao';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, RequestOptions } from '@angular/http';
/**
 * Generated class for the CadastroUsuarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cadastro-usuario',
  templateUrl: 'cadastro-usuario.html',
})
export class CadastroUsuarioPage {

  validacaoPage = ValidacaoPage;
  constructor(public navCtrl: NavController, public navParams: NavParams,private http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastroUsuarioPage');
  }
  cadastrarUsuario(pnome,unome, email, cpf, senha){
    this.navCtrl.push(ValidacaoPage);

    let postData = JSON.stringify(
        {
          firstname: pnome,
          lastname: unome,
          cpf: cpf,
          email: email,
          password: senha
        }
      
      );
    alert(postData);

    this.http.post("http://192.168.0.14:3000/acc/register", postData)
    .subscribe(data => {
      console.log(data['_body']);
     }, error => {
      console.log(error);
    });
    
  }

}
