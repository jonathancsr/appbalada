import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, RequestOptions } from '@angular/http';

/**
 * Generated class for the FeedbackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html',
})
export class FeedbackPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedbackPage');
  }

  feedback(assunto, email, notas) {

    let postData = JSON.stringify(
      {
        assunto: assunto,
        email: email,
        notas: notas
      }
    );

    alert(postData)

    this.http.post("http://192.168.0.14:3000/feedback", postData)
      .subscribe(data => {
        console.log(data['_body']);
      }, error => {
        console.log(error);
      });


  }
}
