import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { CadastroUsuarioPage } from '../cadastro-usuario/cadastro-usuario';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import * as $ from 'jquery';
import {HttpClientModule} from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
 

/**
 * Generated class for the ValidacaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-validacao',
  templateUrl: 'validacao.html',
})
export class ValidacaoPage {

  homePage = HomePage;
  cadastroUsuarioPage = CadastroUsuarioPage
  
  constructor(public navCtrl: NavController,public navParams: NavParams,private http: Http ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ValidacaoPage');
  }

  validar(token){

    this.navCtrl.push(HomePage);
    let postData = JSON.stringify({token: token});//tem que adicionar email do storage

    this.http.post("http://192.168.0.14:3000/acc/cktoken", postData)
        .subscribe(data => {
          console.log(data['_body']);
          this.navCtrl.push(HomePage)
         }, error => {
          console.log(error);
        });
  }

}
