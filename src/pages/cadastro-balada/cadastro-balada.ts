import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BrMaskerModule } from 'brmasker-ionic-3';
import * as $ from 'jquery';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, RequestOptions } from '@angular/http';

/**
 * Generated class for the CadastroBaladaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 
@NgModule({
  imports: [
    BrMaskerModule
  ],
})

@IonicPage()
@Component({
  selector: 'page-cadastro-balada',
  templateUrl: 'cadastro-balada.html',
})
export class CadastroBaladaPage {
  constructor(public navCtrl: NavController, public navParams: NavParams, private http: Http) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastroBaladaPage');
  }
  cadatrarBalada( nome,email,senha,rua,numero,bairro,cidade,telefone,cnpj){

    let postData = JSON.stringify(//tem que adicionar id do usuario do storage
      {
          name: nome,
          email: email,
          password: senha,
          street: rua,
          number: numero,
          district: bairro,
          city: cidade,
          phone: telefone,
          cnpj:cnpj
        }
      );

      this.http.post("http://192.168.0.14:3000/nclub/register", postData)
    .subscribe(data => {
      console.log(data['_body']);
     }, error => {
      console.log(error);
    });
      
   
  }

}
