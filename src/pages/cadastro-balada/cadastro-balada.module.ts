import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroBaladaPage } from './cadastro-balada';

@NgModule({
  declarations: [
    CadastroBaladaPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastroBaladaPage),
  ],
})
export class CadastroBaladaPageModule {}
