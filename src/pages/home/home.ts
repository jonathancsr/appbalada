import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

declare var google;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  map: any;

  constructor(private geolocation: Geolocation) { }
  
  ionViewDidLoad() {
    // const position = new google.maps.LatLng( -19.923184, -43.944630);
 
    // const mapOptions = {
    //   zoom: 13,
    //   center: position,
    //   disableDefaultUI: true
    // }
    
    // this.map = new google.maps.Map(document.getElementById('map'), mapOptions);
    
    // const marker = new google.maps.Marker({
    //   position: position,
    //   map: this.map,
 
    //   //Titulo
    //   title: 'Minha posição',
 
    //   //Animção
    //   animation: google.maps.Animation.DROP, // BOUNCE
 
    //   //Icone
    //   icon: 'assets/imgs/pessoa.png'
    // });
  }
}
