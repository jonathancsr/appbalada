import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import * as $ from 'jquery';
import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, RequestOptions } from '@angular/http';


/**
 * Generated class for the EventoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-evento',
  templateUrl: 'evento.html',
})
export class EventoPage {

  regData = { avatar:'' };
  imgPreview = 'assets/imgs/blank-avatar.jpg';

  constructor(public navCtrl: NavController, public navParams: NavParams, private imagePicker: ImagePicker, private base64: Base64,private http: Http) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventoPage');
  }

  getPhoto() {
    let options = {
      maximumImagesCount: 1
    };
    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
          this.imgPreview = results[i];
          this.base64.encodeFile(results[i]).then((base64File: string) => {
            this.regData.avatar = base64File;
          }, (err) => {
            console.log(err);
          });
      }
    }, (err) => { });
  }

  cadastrarEvento(nome, preco, dataInicio, dataFim,idadeMin){
    this.navCtrl.push(HomePage);

    let postData = JSON.stringify(//tem que adicionar id da balada do storage
        {
          name: nome,
          ticketprice: preco,
          minimumage: idadeMin,
          startate: dataInicio,
          enddate: dataFim
        }
    );
   
    this.http.post("http://192.168.0.14:3000/acc/newpassword", postData)
    .subscribe(data => {
      console.log(data['_body']);
     }, error => {
      console.log(error);
    });
    
  }

}
