import { Component, TestabilityRegistry } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { CadastroUsuarioPage } from '../cadastro-usuario/cadastro-usuario';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import * as $ from 'jquery';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { HTTP } from '@ionic-native/http';
import {HttpClientModule} from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  homePage = HomePage;
  cadastroUsuarioPage = CadastroUsuarioPage
  
  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams,public httpClient: HttpClient,private http: Http ) {  this.http = http}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


  login(user, senha) {

    /*this.uniqueDeviceID.get()
      .then((uuid: any) => alert(uuid))
      .catch((error: any) => alert(error));*/

   //this.navCtrl.push(HomePage);
    let postData = JSON.stringify({ email: user, password: senha, uuid: "pc_landin", timestamp: new Date().getTime()});

      var headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json' );
      const requestOptions = new RequestOptions({ headers: headers });
  
  
      this.http.post("http://192.168.0.14:3000/acc/login", postData)
        .subscribe(data => {
            alert("CERTO");
            this.navCtrl.push(HomePage)
         }, error => {
          alert("ERRO");
          this.navCtrl.push(HomePage)
          console.log(error);
        });
  
  }
  
  }

